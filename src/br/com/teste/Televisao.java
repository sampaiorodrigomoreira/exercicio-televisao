package br.com.teste;

public class Televisao {
	
	private int canal;
	private int volume;
	

	public Televisao(int canal, int volume) {
		this.volume = volume;
		this.canal = canal;
		
		System.out.println("Televisao Criada");
		System.out.print("Canal atual: " + canal + "\n");
		System.out.print("Volume atual: " + volume + "\n");
	}
	
	public int getVolume() {
		return volume;
	}
	
	public int getCanal() {
		return canal;
	}

}
