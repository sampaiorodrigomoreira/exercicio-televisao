package br.com.teste;

import java.util.Scanner;

public class ControleRemoto {
	
	Televisao televisao;
	int canal;
	int volume;

	public ControleRemoto(Televisao televisao) {
		canal = televisao.getCanal();
		volume = televisao.getVolume();
		
		System.out.println("Controle Criado!");
		
	}

	public void aumentaCanal() {
		canal +=  1;
		System.out.println("Canal: " + canal);
	}
	
	public void aumentaCanalIndicado() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Escolha o canal: ");
		canal = scanner.nextInt();
		System.out.println("Canal: " + canal);
		scanner.close();
	}
	
	public void diminuiCanal() {
		canal -= 1;
		System.out.println("Canal: " + canal);
	}
	
	public void aumentaVolume() {		
		volume += 1;
		System.out.println("volume: " + volume);
	}
	
	public void diminuiVolume() {
		volume -= 1;
		System.out.println("volume: " + volume);
	}
	
	public int getVolume() {
		return volume;
	}
	
	public int getCanal() {
		return canal;
	}
}
